fine = true;
//fine = false;

$fn = fine ? 100 : 25;

use <lib/spool.scad>;
include <lib/values.scad>;


rotate([0, 0, 39]) {

/*
    innie_hub();
    base(color="cyan");
*/

/*    translate([0, 0, core_height]) {
        rotate([180, 0, 0]) {
            outie_hub();
            base();
        }
    }
*/

}


drawer_depth = flange_depth;


module quarter_drawer() {
    rotate([0, 0, 0]) {
        intersection() {
            difference() {
                union() {
                    color("green") rotate_extrude(angle = 80) {
                        translate([(core_diameter / 2) + plate_thickness, plate_thickness, 0]) {
                            square([drawer_depth - plate_thickness, core_height - (plate_thickness * 2)]);
                        }
                    }
                    hull() {
                        rotate([0, 0, 79]) {
                            color("red") rotate_extrude(angle = 1) {
                                translate([(core_diameter / 2) + plate_thickness, plate_thickness, 0]) {
                                    square([drawer_depth - plate_thickness, core_height - (plate_thickness * 2)]);
                                }
                            }
                        }
                        rotate([0, 0, 83.7]) {
                            translate([(core_diameter / 2) + drawer_depth - 10, 0, 0]) {
                                translate([0, 0, plate_thickness]) {
                                    color("cyan") cylinder(d = 20, h = core_height - (plate_thickness * 2));
                                }
                            }
                        }
                    }
                }
                rotate([0, 0, 83.7]) {
                    translate([(core_diameter / 2) + drawer_depth - 10, 0, 0]) {
                        color("midnightblue") cylinder(d = 10, h = core_height);
                    }
                }
            }
              

            color("orange") rotate_extrude(angle = 90) {
                translate([(core_diameter / 2) + plate_thickness, 0, 0]) {
                    square([drawer_depth - plate_thickness, core_height]);
                }
            }
        }
    }
}

            
         
          
intersection() {

    quarter_drawer(); 

/*
    rotate([0, 0, 90]) {
        quarter_drawer();
    }
*/

    //cube([250, 250, 10], center = true);
}



