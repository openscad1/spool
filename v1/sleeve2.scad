core_height = 60;
core_diameter = 101;
core_wall_thickness = 3;

sleeve_wall_thickness = 5;
sleeve_inner_radius = core_diameter / 2;
sleeve_height = core_height;


    



module ring(arc = 360, radius = 30, width = 5, height = 10) {
    rotate_extrude(angle = arc) {
        translate([radius, 0, 0]) {
            polygon(points=[[0,0], [0,height], [width,height], [width, 0]]);
        }
    }
    
}


finger_arc = 45;

$fn = 100; 


translate([0, 0, 0]) {
    color("blue") {
        ring(arc = 180, radius = sleeve_inner_radius, width = sleeve_wall_thickness, height = sleeve_height / 4);
    }
    translate([0, 0, (sleeve_height / 4)]) {
        rotate([0, 0, finger_arc]) {
            color("orange") {
                ring(arc = 180, radius = sleeve_inner_radius, width = sleeve_wall_thickness, height = sleeve_height / 4);
            }
        }
    }
     translate([0, 0, (sleeve_height / 4) * 2]) {
        rotate([0, 0, 0]) {
            color("green") {
                ring(arc = 180, radius = sleeve_inner_radius, width = sleeve_wall_thickness, height = sleeve_height / 4);
            }
        }
    }
    translate([0, 0, (sleeve_height / 4)] * 3) {
        rotate([0, 0, finger_arc]) {
            color("red") {
                ring(arc = 180, radius = sleeve_inner_radius, width = sleeve_wall_thickness, height = sleeve_height / 4);
            }
        }
    }

}
