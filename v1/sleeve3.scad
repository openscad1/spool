// fine
fine = false;

    
$fn = fine ? 100 : 25;

// outer_ring
outer_guide = true;


// arm
arm = true;

// retainer
retainer = true;

module ring(arc = 360, radius = 30, width = 5, height = 10) {
    rotate_extrude(angle = arc) {
        translate([radius, 0, 0]) {
            polygon(points=[[0,0], [0,height], [width,height], [width, 0]]);
        }
    }
    
}

module core(offset = 0) {
    color("Chartreuse") cylinder(d = core_diameter + offset, h = sleeve_height + offset);
}


module inner_sleeve() {
    translate([0, 0, 0]) {
        color("blue") {
            ring(radius = sleeve_inner_radius, width = sleeve_wall_thickness, height = sleeve_height);
        }
    }
}

module shoe() {
    translate([sleeve_inner_radius + core_wall_thickness, 0, 0]) {
        difference() {
            cylinder(d = shoe_diameter, h = core_height);
            translate([0, 0, -1]) {
                cylinder(d = foot_diameter, h = core_height + 2);
                translate([0, 0, core_height / 2]) 
                    cube([shoe_diameter, arm_thickness, core_height + 4], center = true);
            }
        }
    }
}

module outer_sleeve() {
    translate([0, 0, 0]) {
        color("orange") {
            ring(radius = (spool_diameter / 2) - sleeve_wall_thickness, width = sleeve_wall_thickness, height = sleeve_height);
        }
    }
}


module arm(corner_radius = 1, arm_thickness = 5, hook_width = 10) {
    fudge = 3;
    translate([(spool_diameter / 2) - arm_thickness - (hook_width / 2) - fudge, (hook_width / 2) + arm_thickness, 0]) {
    difference() {
        color("red") 
            translate([0, 0, 0])
                rotate_extrude() 
                    translate([(hook_width / 2), 0, 0]) 
                        square([arm_thickness,sleeve_height]);



        color("green")
            translate([-1 * (arm_thickness + (hook_width / 2)), -1 * (arm_thickness + (hook_width / 2)), 0])
                cube([arm_thickness + (hook_width / 2), (arm_thickness * 2) + hook_width,sleeve_height]);

    }
    }
    translate([(core_diameter / 2) + sleeve_wall_thickness, 0, 0])
        color("pink") 
            cube([((spool_diameter - core_diameter) / 2) - sleeve_wall_thickness - (hook_width / 2) - arm_thickness - fudge, arm_thickness, sleeve_height]);
}

core_height = 60;
core_diameter = 101;
core_wall_thickness = 3;

spool_diameter = (50 * 2) + core_diameter - 1;

sleeve_wall_thickness = 5;
sleeve_inner_radius = core_diameter / 2;
sleeve_height = core_height;

arm_thickness = 5;


if (retainer) {
    inner_sleeve();
color("pink") {
    difference() {
        shoe();
        translate([0, 0, -1])
            core(2);
    }
}
}


if(outer_guide) 
    outer_sleeve();



foot_diameter = 20;
shoe_diameter = 30;


if (arm) {
color("red")
    difference () {
        union() {
            translate([0, -1 * (arm_thickness / 2), 0]) {
                arm();
            }
            translate([(core_diameter / 2) + core_wall_thickness, 0, sleeve_height / 2]) {
                color("lightblue") 
                    cylinder(d = foot_diameter, h = sleeve_height, center = true);
            }
        }
        translate([0, 0, -0.01]) {
            core(sleeve_wall_thickness * 2);
        }

}

}