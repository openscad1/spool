core_height = 60;
core_diameter = 100;
core_wall_thickness = 3;

sleeve_wall_thickness = 10;
sleeve_inner_radius = core_diameter / 2;
sleeve_height = core_height;


    




module inner_sleeve(arc = 180) {
    rotate_extrude(angle = arc) {
        translate([sleeve_inner_radius, 0, 0]) {
            polygon(points=[[0,0], [0,sleeve_height], [sleeve_wall_thickness,sleeve_height], [sleeve_wall_thickness,0]]);
        }
    }

}


module finger(arc = 45) {
    rotate_extrude(angle = arc) {
        translate([sleeve_inner_radius, 0, 0]) {
            polygon(points=[[0,0], [0,(sleeve_height / 2)], [sleeve_wall_thickness,(sleeve_height / 2)], [sleeve_wall_thickness,0]]);
        }
    }
}


finger_arc = 30;

$fn = 100; 
inner_sleeve(arc = 180 - finger_arc);

rotate([0, 0, 180 - finger_arc]) finger(arc = finger_arc);
translate([0, 0, sleeve_height / 2]) rotate([0, 0, -finger_arc]) finger(arc = finger_arc);
