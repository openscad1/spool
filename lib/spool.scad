include <values.scad>;

module spokes() {
    for(i = [0 : 60 : 360]) {
        rotate([0, 0, i]) {
            rotate([90, 0, 0])  {
                cylinder(h = core_diameter + (flange_depth * 3), d = spoke_diameter, center = true);
            }
        }
    }    
}


module base(color = "orange") {
    intersection() {
        difference() {
            color(color) cylinder(d = core_diameter + (flange_depth * 2), h = plate_thickness);
            translate([0, 0, -0.01]) {
                color(color) cylinder(d = core_diameter, h = plate_thickness + 0.02);
            }

  
        } 

        translate([0, 0, (plate_thickness / 2) - 0.01]) {
            color(color) cube([print_plate_maxiumum, print_plate_maxiumum, plate_thickness + 0.02], center = true);
        }

    }
              for(i = [1:4]) {
                rotate([0, 0, 45 + (90 * i)]) {
                    translate([((core_diameter + (flange_depth * 2)) / 2) - 10, 0, 0]) {
                        color(color) cylinder(d = 10, h = plate_thickness * 3);
                        translate([0, 0, plate_thickness * 3]) {
                            color(color) sphere(d = 10);
                        }
                    }
                }
            }
}



module innie_hub(color = "purple") {

    rotate([0, 0, -15]) {
        difference() {
            color(color) cylinder(d = core_diameter + (plate_thickness * 2), h = core_height / 2);
            translate([0, 0, -1]) {
                color(color) cylinder(d = core_diameter, h = core_height + 2);
            }
            translate([0, 0, core_height / 2]) {
                color(color) spokes();
            }
        }
    }
 
    rotate([0, 0, 15]) {
        intersection() {
            translate([0, 0, core_height / 2]) {
                color(color) spokes();
            }

            difference () {
                color(color) cylinder(d = core_diameter + (plate_thickness * 2), h = (core_height / 2) + (spoke_diameter / 2));
                translate([0, 0, -0.01]) {
                    color(color) cylinder(d = core_diameter, h = (core_height / 2) + (spoke_diameter / 2) + 0.02);
                }
            }
        }
    }
}


module outie_hub(color = "salmon") {

    difference() {
        color(color) cylinder(d = core_diameter + (plate_thickness * 2), h = core_height / 2);
        translate([0, 0, -1]) {
            color(color) cylinder(d = core_diameter, h = core_height + 2);
        }
    }

    intersection() {
        translate([0, 0, core_height / 2]) {
            color(color) spokes();
        }

        difference () {
            color(color) cylinder(d = core_diameter + (plate_thickness * 2), h = (core_height / 2) + (spoke_diameter / 2));
            translate([0, 0, -0.01]) {
                color(color) cylinder(d = core_diameter, h = (core_height / 2) + (spoke_diameter / 2) + 0.02);
            }
        }
    }

}


innie_hub();
base(color="cyan");

translate([0, 0, core_height + 1]) {
    rotate([180, 0, 0]) {
        innie_hub(color="yellow");
        base();
    }
}

